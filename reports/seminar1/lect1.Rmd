---
title: "stat4arch: seminar 1"
author: "Petr Pajdla"
date: "21st Feb 2020"
output: 
  ioslides_presentation:
    widescreen: false
# 
#   bookdown::html_document2:
#     theme: spacelab
#     highlight: tango
#     number_sections: no
#     toc: yes
#     toc_depth: 4
#     toc_float: yes
#     fig.caption: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

<!-- ======================================================================= -->

# Úvod

<!-- ----------------------------------------------------------------------- -->

## Cíle kurzu

> - naučit se pracovat v `R`
> - porozumět základním statistickým pojmům
> - vytvořit a interpretovat základní grafy
> - vypočítat a interpretovat míru závislosti mezi proměnnými
> - zvolit a aplikovat vhodnou kombinaci metod pro analýzu vícerozměrných dat
> - připravit vícerozměrná data pro zvolenou metodu analýzy
> - interpretovat výsledky vybraných vícerozměrných statistických analýz

<!-- ----------------------------------------------------------------------- -->

## Cíl dnešní hodiny

Základy práce v R a pokus o vytvoření následujícího grafu:

```{r, echo=FALSE}
library(archdata)
library(ggplot2)
data("Fibulae")
ggplot(data = Fibulae, mapping = aes(x = Length, 
                                     y = BH,
                                     size = Coils)) +
  geom_point(shape = 21) +
  labs(x = "Length", 
       y = "Bow height", 
       title = "Bronze fibulae length by bow height")

# plot(x = Fibulae$Length, y = Fibulae$BH, cex = Fibulae$Coils*0.2,
#      xlab = "Length", ylab = "Bow height",
#      main = "Bronze fibulae length by bow height")
```


<!-- ----------------------------------------------------------------------- -->

## Co je to R?

<img src="./figures/Rlogo.png" width="100">

<!-- > R is a free software environment for statistical computing and graphics.  -->
<!-- > It compiles and runs on a wide variety of UNIX platforms, Windows and MacOS. -->

* skriptovací jazyk a prostředí pro statistické výpočty
* open source, free
* v současnosti standard pro statistické výpočty
* alternativy: Python, SQL, JavaScript atd.
* klikací alternativy: Statistica, PAST atd.

Home page: <https://www.r-project.org/>  
CRAN: <https://mirrors.nic.cz/R/>

<!-- ======================================================================= -->

# Is this statistics?

<img src="./figures/stats_alien.jpeg" align="right" width="240">

<!-- ----------------------------------------------------------------------- -->

## Statistics vs *data science*

*Data science is an inter-disciplinary field that uses scientific methods, processes, algorithms and systems to extract knowledge and insights from structured and unstructured data.*
<https://en.wikipedia.org/wiki/Data_science>

<img src="./figures/ds_vd_skills.png" height="320">

<!-- ----------------------------------------------------------------------- -->

## Exploratory data analysis

<img src="./figures/data-science.png" width="780">

<!-- ======================================================================= -->

# Úvod do R

<img src="./figures/r_meme.jpeg" width="400">

<!-- ----------------------------------------------------------------------- -->

## RStudio

<img src="./figures/rstudio-logo.png" width="200">

* IDE (Integrated Development Environment) pro R
* Free desktop verze

*RStudio is a set of integrated tools designed to help you be more productive with R. It includes a console, syntax-highlighting editor that supports direct code execution, and a variety of robust tools for plotting, viewing history, debugging and managing your workspace.*

<https://rstudio.com/>

<!-- ----------------------------------------------------------------------- -->

## IDE

<img src="./figures/rstudio-ide.png" height="450">

<!-- ----------------------------------------------------------------------- -->

## R Console

<img src="./figures/rstudio-console.png" height="450">

<!-- ----------------------------------------------------------------------- -->

## Kde hledat pomoc?

![](./figures/fry.jpeg)

<!-- ----------------------------------------------------------------------- -->

## Kde hledat pomoc?

* Rstudio Cheat Sheets <https://rstudio.com/resources/cheatsheets/>  
  + Data Import Cheat Sheet
  + Data Transformation Cheat Sheet
  + Data Visualization Cheat Sheet
  + R Markdown Cheat Sheet  
* Stack Overflow  
  <https://stackoverflow.com/questions/tagged/r>  
  + tag `r`  
* Přímo v `R`

<!-- ----------------------------------------------------------------------- -->

## Zdroje - R
### R for Data Science
Hadley Wickham & Garret Grolemund  

<img src="./figures/r4ds_cover.png" height="280">  
<https://r4ds.had.co.nz/>

## Zdroje - R, vizualizace dat
### Data Visualization: A Practical Introduction
Kieran Healy  

<img src="./figures/dv-cover-pupress.jpg" height="280">  
<http://socviz.co/index.html#preface>

## Zdroje - R v archeologii
### Quantitative Methods in Archaeology Using R
David L. Carlson  

<img src="./figures/quantarch.jpg" height="280">

## Zdroje - statistika v archeologii
### Statistics for Archaeologists: A Commonsense Approach
Robert Drennan  

<img src="./figures/drennan.jpg" height="280">

## Zdroje - statistika v archeologii
### Quantifying Archaeology
Stephen Shennan  

<img src="./figures/shennan.jpeg" height="280">

## Zdroje - matematika v archeologii
### Mathematics and Archaeology
Juan A. Barceló & Igor Bogdanovic  

<img src="./figures/barcelo.jpeg" height="280">

<!-- ======================================================================= -->

# Let's practice!

<!-- ----------------------------------------------------------------------- -->

## Tasks

> - Start RStudio
> - Lokalizujte konzoli :)
> - Do konzole napište `demo("graphics")`
> - Cool, right?

> - "Hello, World!" *prográmek*:
> - `print("Hello, World!")`
> - `print("What is your name?")`
> - `my_name <- readline()`
> - `print(paste0("Hello, ", my_name, "!"))`

<!-- ----------------------------------------------------------------------- -->

## R je chytrá kalkulačka...

* Základní matematické operace: `+`, `-`, `*`, `/`
* Exponent: `x^n` (nebo `x**n`)
* Odmocnina: `sqrt()`

```{r}
2 + 40
round(6.48^2)
sqrt(1764)

```

<!-- ----------------------------------------------------------------------- -->

## Note on special characters

<img src="./figures/spec_chars.png" height="280">

<!-- ======================================================================= -->

# Základy R

<img src="./figures/Rlogo.png" align="right" width="200">

<!-- ----------------------------------------------------------------------- -->

## Konzole vs skript

<img src="./figures/rstudio-ide.png" height="250">

* Konzole po odentrování evaluuje odeslaný kód a okamžitě nabízí výstup
* Skript slouží k dokumentaci použitého kódu (= program)
* Skript je *de facto* textový dokument...

`ctrl + enter - pošle do konzole vybraný řádek`

<!-- ----------------------------------------------------------------------- -->

## Funkce vs objekt

* Funkce končí závorkou ()

```{r, eval=FALSE}
function(arguments)
```

```{r}
print("Hello, World!")
```

* Objekt je všechno ostatní

```{r}
muj_objekt <- 21.0
muj_objekt + 21.0
```

`<- - assignment operator`

<!-- ----------------------------------------------------------------------- -->

## Libraries & packages

* Base R má mnoho funkcí samo o sobě
* Rozšiřující funkce nabízí množství balíčků (>15000)
viz CRAN <https://mirrors.nic.cz/R/>

### Instalace dodatečných balíčků z repozitáře CRAN

```{r, eval=FALSE}
install.packages("package name goes here")
```

### Načtení balíčku pro použití z lokální knihovny

```{r, eval = FALSE}
library(package name goes here)
```

<!-- ----------------------------------------------------------------------- -->

## Funkce `c()`

```{r}
c("Fuu", "Bar")
muj_objekt <- c(1, 2, 3, 4:10) # this is a comment
print(muj_objekt)
```

`c() - concatenate/combine`  
`"x" - string (textový řetězec)`  
`: - generate sequence`  
`# - comment`  
`print() - return to console`

## Getting help on functions

```{r eval=FALSE}
help(mean) 
?mean # equivalent to help(mean)
```

```{r}
args(mean) # returns arguments the function can take
mean # function without parentheses prints some details
```


<!-- ======================================================================= -->

# Let's practice!

<img src="./figures/theory_to_ml.jpeg" align="right" width="250">

## Tasks

> - Nainstalujte si package `tidyverse`
> - Načtěte package `tidyverse` z knihovny
> - Vytvořte objekt s libovolným názvem, který bude obsahovat čísla od 20 do 24
> - Nainstalujte package `archdata`, která obsahuje data z knihy *Quantitative Methods in Archaeology Using R*


## Solutions

```{r, eval=FALSE}
install.packages("tidyverse")
library(tidyverse)
```

```{r}
objekt1 <- 20:24 # nebo
objekt2 <- c(20, 21, 22, 23, 24)
print(objekt1)
all(objekt1 == objekt2)
```

```{r, eval = FALSE}
install.packages("archdata")
```

<!-- ======================================================================= -->

# Typy objektů v R

<!-- ----------------------------------------------------------------------- -->

## Vector

* základní objekt v R
* vektor vytvoříme funkcí `c()`
* vektor může obsahovat jen data stejného typu

```{r}
muj_vektor <- c(1:5, 42, 6:10) # this is a vector
muj_vektor
is.vector(muj_vektor)
```

`is.vector() - vrátí TRUE je-li objekt vektor`

<!-- ----------------------------------------------------------------------- -->

## Subsetting vectors

```{r}
muj_vektor[6]
muj_vektor[4:8]
muj_vektor[-6]
muj_vektor[c(2, 4, 7)]
```

<!-- `[n] - vybere n-tou hodnotu vektoru` -->

<!-- ----------------------------------------------------------------------- -->

## Data frame & tidy data

* V podstatě tabulka o `m` sloupcích a `n` řádcích
* Každý sloupec je zároveň `vektor`

<img src="./figures/tidy_data.png" height="200">


### Tidy data

1. Each variable must have its own column.  
2. Each observation must have its own row.  
3. Each value must have its own cell.

<!-- ----------------------------------------------------------------------- -->

## Data frame and tibble v R

```{r, echo=FALSE}
muj_df <- tidyr::tibble(col1 = 1:4, 
           col2 = letters[1:4],
           col3 = LETTERS[26:23])
```

```{r}
print(muj_df)
is.data.frame(muj_df)
```

<!-- ----------------------------------------------------------------------- -->

## Subsetting data frames and tibbles

<!-- # ```{r, eval=FALSE} -->
<!-- # df[m, n] -->
<!-- # ``` -->

```{r}
muj_df[1, ] # row 1
muj_df[, 3] # col 3
```

## Subsetting data frames and tibbles

```{r}
muj_df[4, 1] # row 4, col 1
muj_df$col1 # shorthand to subset exact column
# notice that column is returned as a vector
```

## Useful functions

```{r}
dim(muj_df) # n (row) x m (col) rozměr df
colnames(muj_df) # názvy sloupců
# rownames(muj_df) # názvy řádků
str(muj_df) # struktura objektu
```

<!-- ======================================================================= -->

# Types of data

<!-- <img src="./figures/data_types.jpeg" align="right" width="240"> -->

<!-- ----------------------------------------------------------------------- -->

## Základní typy dat {.build}

> - text (string) - `character` ("Ahoj!")
> - integrál (celé číslo) - `integer` (1, 3, 5)
> - desetinné číslo - `double` (3.14)
> - binární hodnota (boolean value) - `logical` (TRUE/FALSE)

### Speciální hodnoty

> - prázdná hodnota - not available (NA)
> - nečíslo - not a number (NaN)
> - nekonečna - Infinity (-Inf, +Inf)

<!-- ----------------------------------------------------------------------- -->

## Functions for types of data {.smaller}

```{r}
is.character(muj_vektor)
is.integer(muj_vektor)
is.double(muj_vektor)
is.numeric(muj_vektor) # integer i double
```

<!-- ----------------------------------------------------------------------- -->

## Functions for types of data {.smaller}

```{r}
muj_vektor[6] <- NA
print(muj_vektor)
foo <- is.na(muj_vektor) # je jakákoliv hodnota ve vektoru NA?
print(foo)
is.logical(foo)
```

<!-- ----------------------------------------------------------------------- -->

## Comparison in R

`<, <=, ==, >=, >, !=`

```{r}
x <- 1
y <- 100
x <= y
x == 1
x != y
```

<!-- ======================================================================= -->

# Let's practice!

<img src="./figures/practice.png" align="right" width="240">

## Tasks 1

> - restartujte R (`ctrl + shift + F10`)
> - pracujte s nově vytvořeným skriptem
> - skript si uložte někam, kde ho najdete  
    např. "/Dokumenty/stat4arch/seminar1/code/"
> - příkazem `getwd()` proveďte kontrolu, kde *sídlí* váš projekt
> - načtěte knihovny `archdata` a `tidyverse`
> - příkazem `data("Fibulae")` si načtěte dataset `Fibulae`
> - vytiskněte si objekt `Fibulae` do konzole

## Solutions 1

```{r}
library(archdata)
library(tidyverse)

data("Fibulae")
# print(Fibulae)
```

## Tasks 2

> - Co je objekt `Fibulae`?
> - Jaký je počet řádků a sloupců objektu `Fibulae`?
> - Jaké jsou názvy sloupců?
> - Příkazem `?Fibulae` nebo `help(Fibulae)` si projděte dokumentaci k danému datasetu.
> - Který soupec obsahuje délku spon?
> - Který sloupec obsahuje výšku lučíku (bow height)?
> - Který sloupec obsahuje počet vinutí (coils)?

## Solutions 2

```{r}
is.data.frame(Fibulae)
dim(Fibulae) # nebo nrow(Fibulae) a ncol(Fibulae)
colnames(Fibulae)
```

<!-- ======================================================================= -->

# First project

<img src="./figures/fibulae.png" width="600">

## Cíl dnešní hodiny

Pokusit se reprodukovat následující graf:

```{r, echo=FALSE}
ggplot(data = Fibulae, mapping = aes(x = Length, 
                                     y = BH,
                                     size = Coils)) +
  geom_point(shape = 21) +
  labs(x = "Length", 
       y = "Bow height", 
       title = "Bronze fibulae length by bow height")

# plot(x = Fibulae$Length, y = Fibulae$BH, cex = Fibulae$Coils*0.2,
#      xlab = "Length", ylab = "Bow height",
#      main = "Bronze fibulae length by bow height")
```

## Analysis

<img src="./figures/data-science.png" width="400">

> - Máme data?
> - Jaké proměnné (variables) využijeme?
> - Jak z objektu `Fibulae` vytáhnu potřebné proměnné?  
    (dva způsoby)
> - Jak by asi mohla znít funkce pro kreslení grafů?

## Towards plotting

```{r}
head(Fibulae[, c("Length", "BH", "Coils")])
# funkce head() zobrazí jen pár prvních řádků datasetu
```

## Base plot

```{r}
plot(x = Fibulae$Length, y = Fibulae$BH)
```

## Adding title

```{r}
plot(x = Fibulae$Length, y = Fibulae$BH,
     main = "Bronze fibulae length by bow height")
```

## Adding axis labels

```{r}
plot(x = Fibulae$Length, y = Fibulae$BH,
     xlab = "Length", ylab = "Bow height",
     main = "Bronze fibulae length by bow height")
```

## Size of points

```{r}
plot(x = Fibulae$Length, y = Fibulae$BH, cex = Fibulae$Coils,
     xlab = "Length", ylab = "Bow height",
     main = "Bronze fibulae length by bow height")
```

## Correcting the size

```{r}
plot(x = Fibulae$Length, y = Fibulae$BH, cex = Fibulae$Coils*0.2,
     xlab = "Length", ylab = "Bow height",
     main = "Bronze fibulae length by bow height")
```

<!-- ======================================================================= -->

# Summing up

<img src="./figures/dontpanic2.png" height="280">

## Summary

> - Proč používat R?
> - Proč používat skripty?
> - Kde hledat pomoc?
> - Jaký je rozdíl mezi funkcí a objektem?
> - Jaké jsou základní typy objektů v R?
> - Jaké jsou základní typy dat v R?
> - Co je to NA?
> - Jaké jsou principy *tidy* dat?
> - Které funkce si pamatujete?










